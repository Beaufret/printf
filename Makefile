# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/07/09 17:09:36 by rbeaufre          #+#    #+#              #
#    Updated: 2019/09/17 15:29:33 by rbeaufre         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

MAKEFILE = Makefile

BIN_DIR = .
INCLUDES_DIR = .
SRC_DIR = .
OBJ_DIR = obj
INCLUDES_DIR = .

SRC_RAW = ft_atoi.c ft_cs.c ft_data.c ft_di.c ft_flags_to_data.c \
		   ft_o.c ft_p_percent.c ft_padding.c ft_parse_flags.c \
		   ft_printf.c ft_strlen.c ft_xx.c ft_u.c ft_padding2.c \
		   ft_padding_u.c ft_float.c ft_padding_float.c \
		   ft_padding_float2.c ft_float_2.c ft_puttab.c ft_putbits.c \
		   ft_xx_2.c ft_parse_colour.c

SRC = $(addprefix $(SRC_DIR)/, $(SRC_RAW))

OBJ_RAW = $(subst .c,.o,$(SRC_RAW))
OBJ = $(addprefix $(OBJ_DIR)/, $(OBJ_RAW))

HEADERS = ft_printf.h
INCLUDES = $(addprefix $(INCLUDES_DIR)/, $(HEADERS))

CCC = gcc
CFLAGS = -O3 -Wall -Wextra -Werror -I $(INCLUDES)

RED := "\033[0;31m"
GREEN := "\033[0;32m"
CYAN := "\033[0;36m"
RESET :="\033[0m"

all: 	$(NAME)

$(OBJ_DIR):
		@mkdir -p $(OBJ_DIR)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c $(INCLUDES) $(MAKEFILE)
		@$(CCC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJ_DIR) $(OBJ)
		@ar rc $(NAME) $(OBJ)
		@ranlib $(NAME)
		@echo ${GREEN}"Compiled '$(NAME)' with success"${RESET}

clean:
		@rm -f $(OBJ)
		@echo ${CYAN}"Cleaned objects with success"${RESET}
		@rm -rf $(OBJ_DIR)
		@echo ${CYAN}"Cleaned '$(NAME)' objects with success"${RESET}

fclean: clean
		@rm -f $(NAME)
		@echo ${CYAN}"Removed '$(NAME)' with success"${RESET}

re: fclean all

.PHONY: clean fclean
