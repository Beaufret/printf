/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/06 13:27:54 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/09/19 15:20:04 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# define BUFF_SIZE (52)

# include <unistd.h>
# include <stdarg.h>

# define COLOR_H
# define RED "\033[0;31m"
# define GREEN "\033[0;32m"
# define YELLOW "\033[0;33m"
# define BLUE "\033[0;34m"
# define MAGENTA "\033[0;35m"
# define CYAN "\033[0;36m"
# define EOC "\033[0m"

typedef struct	s_data				t_data;
typedef struct	s_long_double_bits	t_long_double_bits;
typedef union	u_double			t_double;

struct	s_data
{
	va_list		ap;
	int			res;
	char		buffer[BUFF_SIZE];
	int			i;
	int			option;
	const char	*converterror;
	int			sharp;
	int			zero;
	int			minus;
	int			space;
	int			plus;
	int			fieldwidth;
	int			precision;
	int			length;
	int			left_spaces;
	int			sign;
	int			zeros;
	int			size;
	int			right_spaces;
	int			apos;
	int			bits;
	int			fd;
};

struct	s_long_double_bits
{
	unsigned long long		mantiss:64;
	unsigned long long		power:15;
	unsigned long long		sign:1;
};

union	u_double
{
	long double					value;
	t_long_double_bits			long_double_bits;
};

int			ft_atoi(const char *str);
int			ft_printf(const char *restrict format, ...);
void		ft_init_data(t_data *data, int fd);
void		ft_reset_data_options(t_data *data);
void		ft_print_data(t_data *data);
char		ft_parse_flags(const char *restrict format, t_data *data, int *i);
void		ft_attrib(const char *restrict str, int *i, t_data *data);
void		ft_fieldwidth(const char *restrict str, int *i, t_data *data);
void		ft_precision(const char *restrict str, int *i, t_data *data);
void		ft_lengthmod(const char *restrict str, int *i, t_data *data);
size_t		ft_strlen(const char *str);
void		ft_fill_buffer(t_data *data, const char *str, unsigned int size);
void		ft_putstr(t_data *data);
void		ft_putchar(t_data *data);
void		ft_putnbr(t_data *data);
void		ft_putnbr_o(t_data *data);
void		ft_putnbr_x(t_data *data);
void		ft_putnbr_u(t_data *data);
void		ft_putnbr_xx(t_data *data);
void		ft_set_padding(t_data *data, long long nb);
void		ft_set_padding_float(t_data *data, long double nb);
void		ft_set_padding_u(t_data *data);
void		ft_put_zeros(t_data *data);
void		ft_put_left_spaces(t_data *data);
void		ft_put_left_spaces_float(t_data *data);
void		ft_put_left_spaces_whole_float(t_data *data);
void		ft_put_right_spaces(t_data *data);
void		ft_put_right_spaces_float(t_data *data);
void		ft_put_sign(t_data *data, long long nb);
void		ft_put_sign_u(t_data *data);
void		ft_put_sign_float(t_data *data, long double db);
void		ft_put_space(t_data *data, long long nb);
void		ft_put_space_u(t_data *data);
void		ft_put_space_float(t_data *data, long double db);
void		ft_percent(t_data *data);
void		ft_p(t_data *data);
void		ft_putlong(long long nb, t_data *data);
void		ft_putlong_x(unsigned long long nb, t_data *data);
void		ft_putlong_xx(unsigned long long nb, t_data *data);
void		ft_putfloat(t_data *data);
void		ft_put_extra_zeros(t_data *data, int prec);
void		ft_putfloat_ext_one(t_data *data, long double db);
void		ft_putfloat_ext_two(t_data *data, long double db, long long nb, int prec);
void		ft_prec_zero(t_data *data, long double db);
void		ft_put_dec(t_data *data, long double db);
void		ft_puttab(t_data *data);
void		ft_putbits_ints(t_data *data, long long nb);
void		ft_putbits_u(t_data *data, unsigned long long nb);
void		ft_parse_colour(t_data *data, const char *format, int *i);
int			ft_strnequ(char const *s1, char const *s2, int n);
void		ft_putbits_float(t_data *data, unsigned long long sign,
		unsigned long long power, unsigned long long mantiss);

#endif
