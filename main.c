/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/06 13:28:06 by rbeaufre          #+#    #+#             */
/*   Updated: 2019/09/19 15:50:40 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdio.h>

int		main(void)
{

	ft_printf("Le {EOC] nombre |{{RED}}%%{{EOC}}| et puis\n{YELLOW}%Bf{EOC}|\n", 1, 0.0);
	/*   printf("%0.10d|\n", -1421546);
	ft_printf("%0.10d|\n", -1421546);
	printf("---\n");

	   printf("%'d|\n", -71421546);
	ft_printf("%'d|\n", -71421546);
	printf("---\n");*/

	return (0);
}
